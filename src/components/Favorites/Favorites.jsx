/* eslint-disable no-useless-constructor */
import { Component } from "react";
import PropTypes from "prop-types";

export class Favorites extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <button type="button" className="btn btn-light border border-secondary position-relative pt-0">
        <img width={20} height={20} src="./img/star-active.png" alt="star" />
        <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
          {this.props.favItemsCount}
        </span>
      </button>
    );
  }
}

Favorites.propTypes = {
  favItemsCount: PropTypes.number,
};
