/* eslint-disable no-useless-constructor */
import { Component } from "react";
import { Favorites } from "../Favorites/Favorites";
import { Cart } from "../Cart/Cart";
import PropTypes from "prop-types";

export class Header extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <nav className="navbar bg-light border-bottom ">
          <div className="container">
            <h1 className="navbar-brand fs-4">Laptops</h1>
            <div className="d-flex gap-4">
              <Favorites favItemsCount={this.props.favItemsCount} />
              <Cart cartItemsCount={this.props.cartItemsCount} />
            </div>
          </div>
        </nav>
      </>
    );
  }
}

Header.propTypes = {
  favItemsCount: PropTypes.number,
  cartItemsCount: PropTypes.number,
};
Header.defaultProps = {
  favItemsCount: 0,
  cartItemsCount: 0,
};
