/* eslint-disable no-useless-constructor */
import { Component } from "react";
import { ItemCard } from "../ItemCard/ItemCard";
import PropTypes from "prop-types";

export class ItemList extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <div className="container">
          <ul className="d-flex justify-content-center mt-3 p-0 flex-wrap gap-3">
            {this.props.items.map((item) => {
              return (
                <ItemCard
                  openToCartModal={this.props.openToCartModal}
                  favourite={this.props.favItems.find((favItem) => favItem.id === item.id) ? true : false}
                  addToFavClick={this.props.addToFavClick}
                  removeFromFavClick={this.props.removeFromFavClick}
                  key={item.id}
                  item={item}
                />
              );
            })}
          </ul>
        </div>
      </>
    );
  }
}

ItemList.propTypes = {
  openToCartModal: PropTypes.func,
  addToFavClick: PropTypes.func,
  removeFromFavClick: PropTypes.func,
  favItems: PropTypes.array,
  items: PropTypes.array,
};
