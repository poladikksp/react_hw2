/* eslint-disable no-useless-constructor */
import { Component } from "react";
import PropTypes from "prop-types";

export class ItemCard extends Component {
  constructor(props) {
    super(props);
  }
  favBtnClick = (e, id) => {
    if (this.props.favourite) {
      this.props.removeFromFavClick(e, id);
    } else {
      this.props.addToFavClick(e, id);
    }
  };
  render() {
    const { id, name, price, imgPath, article, color } = this.props.item;
    return (
      <>
        <li id={id} className="card" style={{ width: "250px" }}>
          <img src={imgPath} className="card-img-top p-3" alt="item-img" />
          <div className="card-body d-flex flex-column justify-content-end">
            <h5 className="card-title">{name}</h5>
            <p className="card-text mb-1">{"Color: " + color}</p>
            <p className="card-text">{"Article: " + article}</p>
            <p className="card-text fw-bold">{price + " UAH"}</p>
            <div className="d-flex justify-content-between">
              <button onClick={(e) => this.favBtnClick(e, id)} className="btn border-0 p-0">
                <img
                  width={25}
                  height={25}
                  src={this.props.favourite ? "./img/star-active.png" : "./img/star.png"}
                  alt="star"
                />
              </button>
              <button onClick={(e) => this.props.openToCartModal(e, id)} className="btn btn-primary">
                Add to cart
              </button>
            </div>
          </div>
        </li>
      </>
    );
  }
}

ItemCard.propTypes = {
  openToCartModal: PropTypes.func,
  addToFavClick: PropTypes.func,
  removeFromFavClick: PropTypes.func,
  item: PropTypes.object,
  favourite: PropTypes.bool,
};
