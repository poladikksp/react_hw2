import { Component } from "react";
import { Header } from "../components/Header/Header";
import { ItemList } from "../components/ItemsList/ItemsList";
import { Modal } from "../components/Modal/Modal";

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { items: [], favItems: [], cartItems: [], modalOpen: false, selectedItem: {} };
  }
  componentDidMount() {
    fetch("./items.json")
      .then((res) => res.json())
      .then((data) => this.setState({ items: data }))
      .catch((error) => console.error(error));
    if (localStorage.getItem("favourites")) {
      this.setState({ favItems: JSON.parse(localStorage.getItem("favourites")) });
    }
    if (localStorage.getItem("cart")) {
      this.setState({ cartItems: JSON.parse(localStorage.getItem("cart")) });
    }
  }
  addToFavClick = (e, id) => {
    const item = this.state.items.find((item) => item.id === id);
    this.setState((prevState) => {
      const newFavArr = [item, ...prevState.favItems];
      localStorage.setItem("favourites", JSON.stringify(newFavArr));
      return { favItems: newFavArr };
    });
  };
  removeFromFavClick = (e, id) => {
    const newFavArr = this.state.favItems.filter((item) => item.id !== id);
    localStorage.setItem("favourites", JSON.stringify(newFavArr));
    this.setState({ favItems: newFavArr });
  };
  addToCart = (selectedItem) => {
    this.setState((prevState) => {
      const newCartArr = [selectedItem, ...prevState.cartItems];
      localStorage.setItem("cart", JSON.stringify(newCartArr));
      return { cartItems: newCartArr };
    });
  };
  openToCartModal = (e, id) => {
    this.setState({ modalOpen: true, selectedItem: this.state.items.find((item) => item.id === id) });
  };
  closeToCartModal = () => {
    this.setState({ modalOpen: false });
  };
  render() {
    return (
      <>
        <Header favItemsCount={this.state.favItems.length} cartItemsCount={this.state.cartItems.length} />
        <ItemList
          openToCartModal={this.openToCartModal}
          addToFavClick={this.addToFavClick}
          removeFromFavClick={this.removeFromFavClick}
          items={this.state.items}
          favItems={this.state.favItems}
        />
        {this.state.modalOpen ? (
          <Modal
            addToCart={this.addToCart}
            selectedItem={this.state.selectedItem}
            closeToCartModal={this.closeToCartModal}
          />
        ) : null}
      </>
    );
  }
}
